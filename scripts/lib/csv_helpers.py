import re
from .default_strings import newline_map


def csv_escape_special_chars(string: str, escape_newline: bool = True) -> str:
    '''
    Purpose: This will convert a string into a CSV compatible string so special characters are not interpreted
    :param string: The requested string for conversion
    :param escape_newline: True = a literal \n character sequence, False = convert to allow newline in same cell
    :return: The CSV compatible string
    '''
    # Escape any lines beginning with an = character
    if string.startswith("=") or re.match(r"[0-9]+[\s]*[%]", string):
        string = "\'" + string
    # Escape any newlines in the string so they are not interpreted as a new row when opening in MS Excel
    if escape_newline:
        # In case a literal \n sequence is desired
        string = string.replace("\n", "\\n")
    else:
        # In case a new line is desired within the same cell
        string = string.replace("\n", "\r\n")
    # Escape any double quotes in the string so they are rendered as a single double quote in MS Excel
    string = string.replace("\"", "\"\"")
    return string


def stringify_list_for_cell(input_list: list, escape_newline: bool = True) -> str:
    '''
    Purpose: This takes a list of items and ensures any necessary characters are escaped for CSV files so the list
    contents will appear in the same cell/column. It uses a special new line sequence so each item in the list is
    displayed in a new line within the cell/column and not on a new row.
    :param input_list: The list of items to covert to a string.
    :param escape_newline: Determines if the newline should be literal \n characters or a newline within the cell.
    :return: The string version of the list in CSV compatible format that remains in the same cell/column
    '''
    count = 0
    output_string = ""
    for item in input_list:
        # Convert each item into its CSV compatible string equivalent
        output_string += csv_escape_special_chars(f"{item}", escape_newline)
        count += 1

        if count < len(input_list):
            # Only add a new line if this is not the last item
            output_string += newline_map

    return output_string


def stringify_list_for_cols(input_list: list) -> str:
    """
    Purpose: This takes a list of items and ensures any necessary characters are escaped for CSV files and converts the
    list into a string such that each item in the list appears in a new column.
    :param input_list: The list of items to covert to a string.
    :return: The string version of the list in CSV compatible format that appears in new columns
    """
    csv_str = "\"{row}\"".format(row="\",\"".join([csv_escape_special_chars(x) for x in input_list]))
    return "" if csv_str == "\"\"" else csv_str


def add_csv_content(delim: str, content: str) -> str:
    """
    Purpose: This adds a specified delimiter with the specified content of a string.
    :param delim: The delimiter to use; for CSV files, a comma is typical
    :param content: The string to add; to the row (for example)
    :return: The formatted string
    """
    return f"{delim}{content}"
