import re
import os
import json
import shutil
from colorama import Fore
from .default_strings import warning_str, error_str
from .errors import ErrorCode

msg_er_color = "{0} {1}:".format(Fore.RED + os.path.basename(__file__), error_str + Fore.RESET)
msg_er = "{0} {1}:".format(os.path.basename(__file__), error_str)
msg_wn_color = "{0} {1}:".format(Fore.YELLOW + os.path.basename(__file__), warning_str + Fore.RESET)
msg_wn = "{0} {1}:".format(os.path.basename(__file__), warning_str)
msg_gd_color = "{0}:".format(Fore.GREEN + os.path.basename(__file__) + Fore.RESET)
msg_gd = "{0}:".format(os.path.basename(__file__))


def unicode_to_ascii(string: str) -> str:
    '''
    Purpose: Converts unicode characters to an ASCII equivalent, where possible.
    :param string: The string to be converted
    :return: The converted string
    '''
    # Converts unicode characters that are out of the ASCII range to the ASCII equivalent
    string = string.replace("\u2012", "-")  # Converts dash to dash
    string = string.replace("\u2013", "--")  # Converts double dash to 2 dashes
    string = string.replace("\u2014", "--")  # Converts double dash to 2 dashes
    string = string.replace("\u2018", "\'")  # Converts slanted single quote to a straight quote
    string = string.replace("\u2019", "\'")  # Converts slanted single quote to a straight quote
    string = string.replace("\u201b", "\'")  # Converts slanted single quote to a straight quote
    string = string.replace("\u201c", "\"")  # Converts slanted double quote to a straight quote
    string = string.replace("\u201d", "\"")  # Converts slanted double quote to a straight quote
    string = string.replace("\u2190", "<-")  # Converts left arrow to less than dash (<-)
    string = string.replace("\u2192", "->")  # Converts right arrow to dash greater than (->)
    string = string.replace("\u2026", "...")  # Converts ellipse to 3 dots
    return string


def is_number(string: str) -> bool:
    '''
    Purpose: Determines if the input str represents a number.
    :param string: The string to check
    :return: True if the string is a representation of a number; False if not
    '''
    # Determine if the input string is a number in the form of: 1 or 1.234, whether positive or negative
    try:
        float(string)
        return True
    except ValueError:
        return False


def custom_sort(list1: list) -> list:
    '''
    Purpose: Takes a string sorted list and fixes numerical increments at the end of folders so they're numerically
    sorted. Note, this only works with a dash or underscore separation between the ending number and the folder name.
    :param list1: An already string sorted list
    :return: A sorted list
    '''
    # The numerically sorted list
    result = []
    # Stores folder file information to be sorted
    files = {}
    no_ending_num = []

    for item in list1:
        # Collect path data and create a list of folder numerical incrementation for each repeated base folder name
        file_name = os.path.basename(item)
        folders = os.path.dirname(item)
        num = ""  # Stores the number, if any, at the end of the string
        num_list = []  # A string representation of each digit in the number

        try:
            # Check to see if the last string segment is a number
            folder_len = len(folders)
            while folder_len > 0:
                # Keep checking characters, starting from the end, until there's none left or a non-digit is reached
                folder_len -= 1
                num_list.insert(0, str(int(folders[folder_len])))
        except ValueError:
            # In case a non-digit character is reached, stop adding characters to num_list and continue
            pass

        if len(num_list) > 0:
            # In case we have numbers, convert the list string representation into an int
            num = int("".join(num_list))

        if folders.rstrip(str(num)) not in files:
            # In case this is the first occurrence, create a key with the base name (without the trailing number)
            files[folders.rstrip(str(num))] = {folders: file_name, "count": []}
        else:
            files[folders.rstrip(str(num))].update({folders: file_name})

        # Add each number to the count list for easy sorting
        files[folders.rstrip(str(num))]["count"].append(num)

    for key, value in files.items():
        # Sort each base name by the list of incremental numbers
        if len(value["count"]) > 0:
            # In case there's paths with numbers on the end of the folders, sort the list numerically
            empty_strs = []
            for thing in value["count"]:
                if '' == thing:
                    empty_strs.append(thing)
                    value["count"].remove(thing)
            value["count"].sort()
            if len(empty_strs) > 0:
                value["count"] = empty_strs + value["count"]
            for num in value["count"]:
                # Add each folder according to the numerically sorted list
                for folder, jfile in value.items():
                    if folder != "count" and re.search("[^0-9]{0}$".format(num), folder) is not None:
                        # In case the current folder ends with the current number and is not the key value 'count'
                        # Should match whole number only, not partial (1 matches only 1 not 21, 31, etc.)
                        result.append("{0}/{1}".format(folder, jfile))
        else:
            # For those folders without numerical endings
            for folder, jfile in value.items():
                if folder != "count":
                    # In case the folder is not the key value 'count'
                    result.append("{0}/{1}".format(folder, jfile))
    return result


def write_str_totext(file_data: str, file_path: str, mode: str = "w") -> dict:
    '''
    Purpose: Writes a string to the specified file_path
    :param file_data: The string to be written to disk
    :param file_path: The file path to save the file_data
    :param mode: The mode used during writing; must be valid modes according to open(), default is 'w'
    :return: status: Success = 0 as the only key; otherwise, keys indicating the error with their messages
    '''
    global msg_gd, msg_gd_color, msg_wn, msg_wn_color, msg_er, msg_er_color
    status = {}
    if "r" in mode.lower():
        # In case a reading mode was entered
        msg = "please use a valid writing mode; refer to open() documentation"
        print("{0} {1}".format(msg_er_color, msg))
        status[ErrorCode.INVALID_INPUT.value] = "{0} {1}".format(msg_er, msg)
        return status
    try:
        with open(file_path, mode) as this_file:
            this_file.writelines(file_data)
    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        msg = "Unable to open {0} for writing.\n({1}){2}"
        print("{0} {1}".format(msg_er_color, msg.format(Fore.RED + file_path + Fore.RESET, err.errno, err)))
        status[err.errno] = "{0} {1}".format(msg_er, msg.format(file_path, err.errno, err))
    else:
        # In case the file was written successfully
        msg = "Wrote {0} successfully."
        print("{0} {1}".format(msg_gd_color, msg.format(Fore.GREEN + file_path + Fore.RESET)))
        status[ErrorCode.SUCCESS.value] = "{0} {1}".format(msg_gd, msg.format(file_path))
    finally:
        return status


def write_dict_tojson(file_data: dict, file_path: str, mode: str = "w", indent: int = 4) -> dict:
    '''
    Purpose: Writes a dictionary as a json file to the specified file_path
    :param file_data: The dictionary to be written to disk in json format
    :param file_path: The file path to save the file_data
    :param mode: The mode used during writing; must be valid modes according to open(), default is 'w'
    :param indent: The amount of indentation to use in the json file; the default is 4
    :return: status: Success = 0 as the only key; otherwise, keys indicating the error with their messages
    '''
    global msg_gd, msg_gd_color, msg_wn, msg_wn_color, msg_er, msg_er_color
    status = {}
    if "r" in mode.lower():
        # In case a reading mode was entered
        msg = "please use a valid writing mode; refer to open() documentation"
        print("{0} {1}".format(msg_er_color, msg))
        status[ErrorCode.INVALID_INPUT.value] = "{0} {1}".format(msg_er, msg)
        return status
    try:
        json.dump(file_data, open(file_path, mode), indent=indent)
    except json.JSONDecodeError as err:
        msg = "Unable to open {0} for writing.\n{1}"
        print("{0} {1}".format(msg_er_color, msg.format(Fore.RED + file_path + Fore.RESET, err)))
        status[ErrorCode.JSON_DECODE_ERROR.value] = "{0} {1}".format(msg_er, msg.format(file_path, err))
        return status
    except (FileNotFoundError, FileExistsError, OSError, IOError) as err:
        msg = "Unable to open {0} for writing.\n({1}){2}"
        print("{0} {1}".format(msg_er_color, msg.format(Fore.RED + file_path + Fore.RESET, err.errno, err)))
        status[err.errno] = "{0} {1}".format(msg_er, msg.format(file_path, err.errno, err))
    else:
        # In case the file was written successfully
        msg = "Wrote {0} successfully."
        print("{0} {1}".format(msg_gd_color, msg.format(Fore.GREEN + file_path + Fore.RESET)))
        status[ErrorCode.SUCCESS.value] = "{0} {1}".format(msg_gd, msg.format(file_path))
    finally:
        return status


def download_file(url: str, outfile: str):
    """
    Purpose: Downloads a single file from a requested URL.
    :param url: The URL pointing to a file that can be downloaded
    :param outfile: The name to use for the downloaded file
    :return: None
    """
    os.system(f"curl -o {outfile} {url}")


def extract_tar_gz(path: str, chdir: str):
    """
    Purpose: Ths extracts tar.gz compressed files so the contents can be used.
    :param path: The path to the tar.gz file needing decompressed
    :param chdir: The directory to change to when performing decompression; the contents will be placed in this folder
    :return: None
    """
    if chdir == "":
        chdir = "."  # In case this is a null string
    if path.endswith(".tar.gz"):
        os.system(f"tar -xf {path} -C {chdir}")
    else:
        msg = f"{path} must be a tar.gz file"
        raise ValueError(msg)


def get_dirs_from_targz_url(compressed_url: str) -> (str, str):
    """
    Purpose: Determines the folder name used by GitLab when creating the tar.gz downloadable file as well as the name
    of the repository directories needed to get to the same location as the downloadable tar.gz file.
    :param compressed_url: The URL to a downloadable tar.gz file
    :return: name (the tar.gz folder name when uncompressed), repo_path (the repo path to the tar.gz file)
    """
    if "?" not in compressed_url or compressed_url.split("?")[1] == "":
        raise ValueError("Unable to process due to the input URL missing parameters")
    # Strip any parameters added to the url; then, remove the gz and tar extensions respectively
    name = os.path.splitext(os.path.splitext(os.path.basename(compressed_url.split("?")[0]))[0])[0]
    # Collect the parameters in a list
    params = str(compressed_url.split("?")[1]).split("&")
    repo_path = ""
    for param in params:
        if param.startswith("path="):
            # Covert repo path from dir1/dir2 to dir1-dir2
            repo_path = param.split("=")[1]
            dir_list = os.path.split(repo_path)
            if dir_list[0] == "" and len(dir_list) > 1:
                dir_list = dir_list[1:]
            name = f"{name}-{'-'.join(dir_list)}"
    return name, repo_path


def dnload_extract(compressed_url: str, base_dir: str):
    """
    Purpose: This will download a tar.gz file from the input compressed_url, which is a GitLab tar.gz downloadable file,
    and extract the contents to the input base_dir. The folder created by GitLab to contain the compressed files is
    removed as well as the tar.gz file after extraction.
    :param compressed_url: The URL to a downloadable tar.gz file
    :param base_dir: The directory to store the uncompressed files
    :return: None
    """
    targz_file = os.path.basename(compressed_url.split("?")[0])
    download_file(compressed_url, os.path.join(base_dir, targz_file))
    extract_tar_gz(os.path.join(base_dir, targz_file), base_dir)  # Extract the files to the base_dir
    # Get the folder name used to store compressed files and the repo path separately
    comp_name, repo_path = get_dirs_from_targz_url(compressed_url)
    if not os.path.exists(os.path.join(base_dir, os.path.dirname(repo_path))):
        os.makedirs(os.path.join(base_dir, os.path.dirname(repo_path)))
    # Move the files to the same location stored in the repo
    os.rename(os.path.join(base_dir, comp_name, repo_path), os.path.join(base_dir, repo_path))
    shutil.rmtree(os.path.join(base_dir, comp_name))  # Since this is now empty
    os.remove(os.path.join(base_dir, targz_file))  # Since the tar.gz was decompressed


def get_data(json_path: str, log: list) -> dict:
    """
    Purpose: This loads a JSON file into a dictionary object.
    :param json_path: The path to a JSON file.
    :param log: A list representing errors to keep track of
    :return: json_data - the dictionary representation of the file located at json_path
    """
    json_data = {}
    try:
        with open(json_path, "r") as json_fp:
            json_data = json.load(json_fp)
    except (json.JSONDecodeError, FileNotFoundError, OSError) as err:
        msg = "{error} Unable to open '{json_path}': {err_msg}"
        print(msg.format(error=msg_er_color, json_path=Fore.CYAN + json_path + Fore.RESET, err_msg=err))
        log.append(msg.format(error=msg_er, json_path=json_path, err_msg=err))
    return json_data


def lstrip_nonalpha(in_str: str) -> str:
    """
    Purpose: This strips any leading non-alpha characters and returns the resulting string.
    :param in_str: The string to check for leading non-alpha characters
    :return: in_str - The string without leading non-alpha characters
    """
    for index, curr_char in enumerate(in_str):
        if curr_char.isalpha():
            return in_str[index:]
    return in_str  # For cases where this is a null string
